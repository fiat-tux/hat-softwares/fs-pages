# FsPages' authors

## Main developer

* Luc Didry, aka Sky (<https://fiat-tux.fr>), core developer, @framasky on [Diaspora*](https://framasphere.org/public/framasky) and on [Twitter](https://twitter.com/framasky)

## Contributors

* Quentin Pagès, occitan translation
