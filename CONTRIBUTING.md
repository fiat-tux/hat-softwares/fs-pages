You can contribute to FsPages by [reporting issues](https://framagit.org/luc/fs-pages/issues) (patches are welcome).

You can also help to improve internationalization:

```
cd lib/FsPages/I18N/
cp en.po de.po
```

Then edit the `de.po` and submit a merge request :-)