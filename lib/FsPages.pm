# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package FsPages;
use Mojo::Base 'Mojolicious';
use Mojo::JSON qw(decode_json);
use Mojo::Util qw(slurp);
use Mojo::URL;
use File::Path qw(make_path remove_tree);
use File::Copy;
use File::Spec;
use Cwd;
use IPC::Run 'run';

mkdir('tmp', 0700) unless (-d 'tmp');
sub startup {
    my $self = shift;

    # Plugins
    $self->plugin('I18N');
    $self->plugin('DebugDumperHelper');
    $self->plugin('Minion' => { SQLite => 'sqlite:minion.db' });
    $self->plugin('Config' =>
        default => {
            git        => '/usr/bin/git',
            repo_dir   => '/var/opt/gitlab/git-data/repositories/',
            pages_dir  => './pages/pages/',
            master_dir => './pages/master/',
            mail       => {
                how    => 'sendmail'
            },
        }
    );
    die "You need to provide a pages_url setting in fs_pages.conf!" unless (defined($self->config('pages_url')));
    die "You need to provide a mail_sender setting in fs_pages.conf!" unless (defined($self->config('mail_sender')));

    # Mail config
    my $mail_config = {
        type     => 'text/plain',
        encoding => 'quoted-printable',
        how      => $self->config('mail')->{'how'}
    };
    $mail_config->{howargs} = $self->config('mail')->{'howargs'} if (defined $self->config('mail')->{'howargs'});

    $self->plugin('Mail' => $mail_config);


    # Minions
    $self->app->minion->add_task(
        build => sub {
            my $job    = shift;
            my $gplace = shift;
            my $mail   = shift;
            my $addr   = shift;

            $job->app->log->info("Starting copy job for $gplace.");

            my @url          = split(/\//, $gplace);
            $url[0]          = lc($url[0]);
            my $temp_url     = Mojo::URL->new($job->app->config('pages_url'));
            my $page_url     = Mojo::URL->new()->scheme($temp_url->scheme)->host($url[0].'.'.$temp_url->host)->path(join('/', ($temp_url->path, $url[1])));
            my $success_text = "Your content should be available at $page_url/";

            my $build_path = File::Spec->catdir('./tmp', $url[0], $url[1]);
            my $repo_dir   = File::Spec->catdir($job->app->config('repo_dir'), $gplace.'.git');
            my $repo       = 'file://'.$repo_dir;
            my $place      = File::Spec->catdir($job->app->config('pages_dir'), $url[0], $url[1]);

            make_path $build_path unless (-d $build_path);
            my $tmp = File::Spec->catdir($job->app->config('pages_dir'), $url[0]);
            make_path $tmp unless (-d $tmp);

            $build_path = File::Spec->catdir($build_path, 'build/');
            remove_tree $build_path if (-d $build_path);

            my @git = ($job->app->config('git'), 'clone', '--depth', '1', $repo, '--branch', 'fs-pages', '--single-branch', $build_path);
            my $gitout;
            if (run \@git, '>&', \$gitout) {
                my $dir = $build_path;
                my $json_file = File::Spec->catfile($dir, '.fs-pages.json');
                my $json;
                # Get config from repository
                if (-f $json_file) {
                    $json = decode_json slurp($json_file);

                    # Do we copy the whole repo or just a folder?
                    if (defined $json->{directory_to_copy}) {
                        my $dir2cp = $json->{directory_to_copy};
                        $dir2cp =~ s/\.\.//g;
                        $dir = File::Spec->catdir($dir, $dir2cp);
                    }

                    # Is this repository the master repo? (eg placed at the root of the site?)
                    if (defined $json->{is_master_repo} && $json->{is_master_repo}) {
                        $place        = File::Spec->catdir($job->app->config('master_dir'), $url[0]);
                        $page_url     = Mojo::URL->new()->scheme($temp_url->scheme)->host($url[0].'.'.$temp_url->host)->path($temp_url->path);
                        $success_text = "Your content should be available at $page_url/";
                    }
                }
                if (-d $dir) {
                    if (-d $place) {
                        remove_tree $place;
                    }
                    move $dir, $place;
                    $job->app->log->info("Copy of $dir to $place done.");
                    $job->app->mail(
                        from    => $job->app->config('mail_sender'),
                        to      => $addr,
                        subject => '[FsPages]['.$gplace.'] Site copy done :-)',
                        data    => $success_text
                    ) if $mail;
                } else {
                    my $msg = "Unable to copy the files from the repository $gplace. Please contact the administrator.";
                    $msg .= "\nYou specified the folder ".$json->{directory_to_copy}.". Does it exists?" if defined $json->{directory_to_copy};
                    $job->app->log->info("Unable to copy the folder $dir of repository $gplace.");
                    $job->app->mail(
                        from    => $job->app->config('mail_sender'),
                        to      => $addr,
                        subject => '[FsPages]['.$gplace.'] ERROR :-(',
                        data    => $msg
                    );
                }
                remove_tree $build_path;
            } else {
                $job->app->log->info("Git clone failed ($?) for repo $gplace: $!\n$gitout");
                $job->app->mail(
                    from    => $job->app->config('mail_sender'),
                    to      => $addr,
                    subject => '[FsPages]['.$gplace.'] ERROR :-(',
                    data    => "Git clone of repository $gplace failed. Please contact the adminstrator."
                );
            }
        }
    );

    # Router
    my $r = $self->routes;

    $r->get('/' => sub {
        shift->render(
            layout   => undef,
            template => 'index'
        );
    });

    # Normal route to controller
    $r->post('/' => sub {
        my $c       = shift;
        my $msg     = decode_json($c->req->content->asset->slurp);
        my $mail    = $c->param('mail');

        $mail = 1 unless defined $mail;

        my $content_dir = $msg->{project}->{path_with_namespace};
        my $addr        = $msg->{user_email};

        $c->app->log->info('Enqueuing repo '.$content_dir);
        $c->app->minion->enqueue(build => [$content_dir, $mail, $addr]);

        $c->render(json => 'ok');
    });
}

1;
