# FsPages

![FsPages logo](https://framagit.org/luc/fs-pages/raw/master/public/img/Panama-Hat.png)

## Warning!!!

As Gitlab pages are now [integrated in Gitla CE](https://about.gitlab.com/2017/02/22/gitlab-8-17-released/), this project is abandoned.

## What is it?

FsPages is a small webservice that allow to publish sites from a Gitlab instance. Yes, quite like Gitlab or Github pages :-)

## License

FsPages is a free/libre software. Its license is the GNU Affero GPLv3. See the [LICENSE file](LICENSE).

## Installation

See the [INSTALL.md file](INSTALL.md).

## Authors

See the [AUTHORS.md file](AUTHORS.md).

## Contributing

See the [CONTRIBUTING.md file](CONTRIBUTING.md).

## Others projects dependencies

FsPages is written in Perl with the [Mojolicious](http://mojolicio.us) framework.

It uses:

- a subset of [Milligram](http://milligram.github.io), a CSS framework
- a few Perl modules. Have a look at the [cpanfile file](cpanfile) to see which
- [Panama Hat](https://openclipart.org/detail/204533/panama-hat) from [netalloy](https://openclipart.org/user-detail/netalloy) (public domain licensed) as logo
