XGETTEXT=carton exec local/bin/xgettext.pl
EXTRACTFILES=utilities/locales_files.txt
EN=lib/FsPages/I18N/en.po
FR=lib/FsPages/I18N/fr.po
CARTON=carton exec
FSPAGES= script/fs_pages

locales:
	$(XGETTEXT) -W -f $(EXTRACTFILES) -o $(EN) 2>/dev/null
	$(XGETTEXT) -W -f $(EXTRACTFILES) -o $(FR) 2>/dev/null
dev:
	$(CARTON) morbo $(FSPAGES) --listen http://0.0.0.0:3000

devlog:
	multitail log/development.log

minion:
	$(CARTON) $(FSPAGES) minion worker -- -m development
